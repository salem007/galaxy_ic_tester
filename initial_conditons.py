import numpy as np
import matplotlib.pyplot as plt 
from scipy.interpolate import interp1d
import astro.galaxy as gal
import astro.cgs as cgs

# set up 2D mesh in cylindrical coords
N = 1000
r_min = z_min = -20.0
r_max = z_max = 20.0
r = np.linspace(r_min,r_max,N)*cgs.kpc
z = np.linspace(z_min,z_max,N)*cgs.kpc
R,Z = np.meshgrid(r,z)
R_SPHERE = np.sqrt( R**2 + Z**2 )

# compute disk gas density
gas_density = gal.gas_disk_density(R,Z)

# compute halo gas density
r0 = 2.0*cgs.kpc
T0 = interp1d(r,gal.halo_gas_temperature(r,M=gal.burkert_mass(r),gamma=1.0))(r0)
new_halo_density = gal.lmc_halo_gas_density(R_SPHERE,r0=r0,T0=T0,
                   M=gal.burkert_mass(R_SPHERE),gamma=1.0) # equipartition

# set sim density to max of disk or halo density
cut = gas_density < new_halo_density
new_density = gas_density
new_density[cut] = new_halo_density[cut]

old_density = gal.gas_disk_density(R,Z)
cutoff = 1.84562780554e-29
old_density[old_density < cutoff] = cutoff


# Plot 2D slice of IC's.
f,((ax1,ax2)) = plt.subplots(1,2)
ax1.imshow(np.log10(new_density))
i2 = ax2.imshow(np.log10(old_density))
#image_plot(gas_density,extent=[r_min,r_max,z_min,z_max]) 
#plt.xlabel('r [ kpc ]')
#plt.ylabel('density [ cgs ]')
plt.savefig('figures/initial_conditions')


# Plot 1D profiles rising out of disk plane.
plt.clf()
labels = []
indices = [500,550,600,800]
for idx in indices:
	labels.append( "R = %.2f kpc" % (r[idx]/cgs.kpc) )
	plt.semilogy(z/cgs.kpc,gas_density[:,idx])

for idx in indices:
	density = gal.gas_disk_density(r[idx],z)
	cutoff = 1e-28
	density[ density < cutoff ] = cutoff
	plt.semilogy(z/cgs.kpc,density,'k--')
plt.legend(labels,loc='best')
plt.xlabel('z [ kpc ]')
plt.ylabel('density [ cgs ]')
plt.savefig('figures/density_comparison.eps')
