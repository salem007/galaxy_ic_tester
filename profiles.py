"""
	Produces 1D profiles for galaxy simulation initial conditions,
	with default parameters chosen to match our RPS of the LMC disk
	simulations. Important references found in the galaxy module
	imported with this script ...
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
import astro.cgs as cgs
import astro.galaxy as gal

# set up radius
N = 100
r_min = 1.0
r_max = 20.0
r = np.linspace(1.0,20.0,100)*cgs.kpc

# Find dark matter mass enclosed
M = gal.burkert_mass(r)

# calculate temperature profile (assumes KE = 1/2 PE locally)
kwargs = { 'M' : M , 'gamma' : 1.0 }
T = gal.halo_gas_temperature(r,**kwargs)

# Find density (hydrostatic equilibrium)
r0 = 2.0*cgs.kpc
T0 = interp1d(r,T)(r0)
print T0
rho = gal.lmc_halo_gas_density(r,r0=r0,T0=T0,**kwargs)

# And finally pressure (ideal gas law)
P = rho * cgs.kb * T / ( cgs.mu * cgs.mp )


# Plot the profiles
f,((ax1,ax2),(ax3,ax4)) = plt.subplots(2,2)

plots = { 
	'Mass (DM)'  : ( ax1 , M/cgs.Msun ),
	'Temperatre' : ( ax2 , T ),
	'Density'    : ( ax3 , rho),
	'Pressure'   : ( ax4 , P )
}

for k,v in plots.items():
	(ax,y) = v
	ax.semilogy(r/cgs.kpc,y,'k-')
	ax.set_title(k)
	ax.set_xlim(1.0,20.0)

for ax in (ax3,ax4):
	ax.set_xlabel('r [kpc]')

# Plot uniform values of old halo
Tu = 1199219.3517; du = 1.84562780554e-29
Pu = du*cgs.kb*Tu/(cgs.mu*cgs.mp)
uniforms = { ax2:Tu,ax3:du,ax4:Pu } 
for ax,y in uniforms.items():
	ax.plot([1.0,20.0],np.ones(2)*y,'k-.',linewidth=.5)

plt.savefig('figures/profiles.eps')
