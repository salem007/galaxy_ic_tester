/*
 * GALAXY
 *
 *		Functions from the enzo galaxy initializer that I've created
 *		or heavily altered, and some bare bones infrastructure to 
 *		implement and test them. The outputs are checked against
 *		the (much easier to develop) python analsyis modules.
 */

#include <iostream>
#include <math.h>
#include <string>

#define min(A,B) ((A) < (B) ? (A) : (B))

#define Mpc 3.0856e24         //Mpc [cm] 
#define SolarMass 1.989e33    //Solar Mass [g]
#define GravConst 6.67e-8     //Gravitational Constant [cm3g-1s-2]
#define pi 3.14159
#define mh 1.67e-24           //Mass of Hydrogen [g]
#define kboltz 1.381e-16      //Boltzmann's Constant [ergK-1]

using std::string;
using std::endl;
using std::cout;
using std::cerr;

typedef double FLOAT;

void ENZO_FAIL(string mssg){
	cerr << mssg << endl;
	throw 0;
}

int GalaxySimulationGasHalo=1,DiskGravity=1;

double MgasScale = 5e+08,gScaleHeightR=0.0017,gScaleHeightz=0.00034,
	LengthUnits = 1.85141e+23,SmoothRadius=0.00769231,TruncRadius=0.01,
	SmoothLength=0.00230769,densicm=1.84563e-29,GalaxySimulationGasHaloScaleRadius=2.0e-3,
	GalaxySimulationGasHaloDensity=1.8e-27,Ticm=1199219.3517,Picm,
	DensityUnits=1e-27,VelocityUnits=5.86686e+09,TimeUnits=3.1557e+13;

double DiskGravityStellarDiskMass=2.7e+09,DiskGravityStellarDiskScaleHeightR=0.0017,
	DiskGravityStellarDiskScaleHeightz=0.00034,DiskGravityStellarBulgeMass=0.0,
	DiskGravityStellarBulgeR=0.0,DiskGravityDarkMatterR=0.003,
	DiskGravityDarkMatterDensity=3.4e-24;


double DiskPotentialCircularVelocity(FLOAT cellwidth,FLOAT z,FLOAT density,FLOAT &temperature);
double trapzd(double (func)(), double a, double b, int n);
double qromb(double (*func)(double), double a, double b);
void polint(double xa[],double ya[],int n,double x,double *y,double *dy);
double func1(double zint);
double func2(double zint);
double func3(double zint);
double func4(double zint);
static double drcyl;
static double r2;


double DiskPotentialDarkMatterMass(FLOAT R){
	FLOAT R0 = DiskGravityDarkMatterR*Mpc,x=R/R0*LengthUnits;
	double M0 = pi*DiskGravityDarkMatterDensity*R0*R0*R0;
	
	return M0*(-2.0*atan(x)+2.0*log(1+x)+log(1.0+x*x));
} // end DiskPotentialDarkMatterMass

float HaloGasTemperature(FLOAT R){
	if(GalaxySimulationGasHalo)
		return GravConst*DiskPotentialDarkMatterMass(R)*0.6*mh/(3.0*kboltz*R*LengthUnits);
	return Ticm;
}

float DiskPotentialGasDensity(FLOAT r,FLOAT z){
  double density = MgasScale*SolarMass/(8.0*pi*pow(gScaleHeightR*Mpc,2)*gScaleHeightz*Mpc);
  density /= (cosh(r*LengthUnits/gScaleHeightR/Mpc)*cosh(z*LengthUnits/gScaleHeightz/Mpc));

  if(fabs(r*LengthUnits/Mpc) > SmoothRadius && fabs(r*LengthUnits/Mpc) <= TruncRadius)
    density *= 0.5*(1.0+cos(pi*(r*LengthUnits-SmoothRadius*Mpc)/(SmoothLength*Mpc)));
  return density;
}

float HaloGasDensity(FLOAT R){
	if(GalaxySimulationGasHalo){
		double T0,haloDensity;
		T0 = HaloGasTemperature(GalaxySimulationGasHaloScaleRadius*Mpc/LengthUnits);
		haloDensity = GalaxySimulationGasHaloDensity*(T0/HaloGasTemperature(R));
		haloDensity /= pow((R*LengthUnits/GalaxySimulationGasHaloScaleRadius/Mpc),3);
		return min(haloDensity,GalaxySimulationGasHaloDensity);
	}
	return densicm;
} // end HaloGasDensity


double findZicm(FLOAT r){
  /*  
   *  Finds the height above the disk plane where the disk gas density
   *  matches the halo's gas density (using bisection)
   */

  static const double X_TOL = 1e-7*Mpc/LengthUnits; // sub pc resolution
  static const int MAX_ITERS = 50; int iters=0;

  double z_lo = 0.0,z_hi = 0.01*Mpc/LengthUnits,z_new,f_lo,f_hi,f_new;
  f_hi = DiskPotentialGasDensity(r,z_hi) - HaloGasDensity(sqrt(r*r+z_hi*z_hi)); // -ve
  f_lo = DiskPotentialGasDensity(r,z_lo) - HaloGasDensity(sqrt(r*r+z_lo*z_lo)); // +ve

  if(f_lo < 0.0) return 0.0; // beyond the disk
  if(f_hi > 0.0) ENZO_FAIL("ERROR IN GALAXY INITIALIZE: HALO IS UNDER-PRESSURIZED");

  while(iters++ < MAX_ITERS ){

    z_new = (z_hi+z_lo)/2.0;
    f_new = DiskPotentialGasDensity(r,z_new)
            - HaloGasDensity(sqrt(r*r+z_new*z_new));

    if( fabs(f_new) == 0.0 ) return z_new;
    if( f_new*f_lo > 0.0 ){
      z_lo = z_new; f_lo = f_new;
    }
    else{
      z_hi = z_new; f_hi = f_new;
    }
    if( fabs(z_hi - z_lo) <= X_TOL ) return z_new;
  }

  ENZO_FAIL("ERROR IN GALAXY INITIALIZE: findZicm FAILED TO CONVERGE");
  return -1.0;
}


int main(){


	/*
	 *	TEST DISK GALAXY DENSITY FUNCTION (WORKS NICELY)
	 */
	size_t Nz = 1000, Nr = 5;
	double r = 0.0, z_min = 0.0, z_max = 0.333,
		dz = (z_max - z_min)/(Nz-1.0), z,
		r_min = 0.0, r_max = SmoothRadius*Mpc/LengthUnits, dr=(r_max-r_min)/(Nr-1.0);
	FILE *fp = fopen("data/density.out","w");
	for( size_t i = 0 ; i != Nz; ++i ){
		z = z_min + i*dz;
		fprintf(fp,"%e",z*LengthUnits);
		for( size_t j = 0 ; j != Nr ; ++j ){
			r = r_min + j*dr;
			fprintf(fp,"\t%e",DiskPotentialGasDensity(r,z));
		}
		fprintf(fp,"\n");
	}// end i for
	fclose(fp);

	/*
	 *	TEST BISECTION METHOD FOR FINDING Z_ICM
	 */
	fp = fopen("data/zicm.out","w");
	for( size_t i = 0 ; i != Nr ; ++i ){
		r = r_min + i*dr;
		fprintf(fp,"%e\t%e\n",r*LengthUnits,findZicm(r)*LengthUnits);
	}
	fclose(fp);


	/*
	 *	TEST HALO PROFILES ...
	 */
	fp = fopen("data/halo.out","w");
	r_min = 1.0e-3*Mpc/LengthUnits; r_max = .02*Mpc/LengthUnits; Nr=100; dr=(r_max-r_min)/(Nr-1.0); // spherical
	for( size_t i = 0 ; i != Nr ; ++i ){
		r = r_min + i*dr;
		fprintf(fp,"%e\t%e\t%e\t%e\n",r*LengthUnits,DiskPotentialDarkMatterMass(r),
			HaloGasTemperature(r),HaloGasDensity(r));
	}// end i for
	fclose(fp);

	/*
	 *	TEST DISK POTENTIAL CIRCULAR VELOCITY for TEMPERATURE PROFILE
	 */
	fp = fopen("data/circular.out","w");
	r = 0; Nz=100; z_min = 1.0e-4*Mpc/LengthUnits; 
	z_max = findZicm(r); dz=(z_max-z_min)/(Nz-1.0);
	double density,temperature,velocity;
	for( size_t i = 0; i != Nz - 1 ; ++i ){
		z = z_min + i*dz;
		density = DiskPotentialGasDensity(r,z)/DensityUnits;
		velocity = DiskPotentialCircularVelocity(dz,z*LengthUnits,density,temperature);
		fprintf(fp,"%e\t%e\t%e\t%e\n",z*LengthUnits,density*DensityUnits,
			temperature,velocity*VelocityUnits);
	} // end i for
	r = sqrt( z_max*z_max + r*r );
	fprintf(fp,"%e\t%e\t%e\t%e\n",z_max*LengthUnits,HaloGasDensity(r),
		HaloGasTemperature(r),velocity*VelocityUnits);
	fclose(fp);


	return 0;
}


/* 
 *	DISK POTENTIAL CIRCULAR VELOCITY
 */
double DiskPotentialCircularVelocity(FLOAT cellwidth, FLOAT z, FLOAT density, FLOAT &temperature)
{

	extern double drcyl;
	double func1(double zint);       //(density times Stellar bulge force)
	double func2(double zint);     //(density times stellar disk force)
	double func3(double zint);       //func1 but for r2
	double func4(double zint);      //func2 but for r2

	double Pressure,Pressure2,zicm,zicm2,zicmf=0.0,zsmall=0.0,
		zicm2f=0.0,zint,FdPdR,FtotR,denuse,rsph,vrot,bulgeComp,rsph_icm;

	r2=(drcyl+0.01*cellwidth)*LengthUnits;
	rsph=sqrt(pow(drcyl*LengthUnits,2)+pow(z,2));

	/*	Determine zicm: the height above the disk where rho -> rho_ICM,
	 *	use this to find P_icm and dP_icm  */
	if (fabs(drcyl*LengthUnits/Mpc) <= SmoothRadius) {

		zicm  = findZicm(drcyl)*LengthUnits;
		zicm2 = findZicm(r2/LengthUnits)*LengthUnits;

		if( fabs(z) < fabs(zicm) ){
			bulgeComp = (DiskGravityStellarBulgeMass==0.0?0.0:qromb(func1, fabs(zicm), fabs(z)));
			Pressure= bulgeComp + qromb(func2, fabs(zicm), fabs(z));
			bulgeComp = (DiskGravityStellarBulgeMass==0.0?0.0:qromb(func3, fabs(zicm2), fabs(z)));
			Pressure2= bulgeComp + qromb(func4, fabs(zicm2), fabs(z));
		}  // end |z| < |zicm| if
	}  else {
    if (fabs(drcyl*LengthUnits/Mpc) <= TruncRadius ) {

			zicm  = findZicm(drcyl)*LengthUnits;
			zicm2 = findZicm(r2/LengthUnits)*LengthUnits;

			if ( HaloGasDensity(sqrt(drcyl*drcyl+z*z)) >= DiskPotentialGasDensity(drcyl,z)
					&& fabs(z) < zicm) {
				printf("small density zicm = %g, z = %g\n", zicm/Mpc, z/Mpc);
			} // end small density if

			if (fabs(z) < fabs(zicm)) {

				bulgeComp = (DiskGravityStellarBulgeMass==0.0?0.0:qromb(func1, fabs(zicm), fabs(z)));
				Pressure  = (bulgeComp+ qromb(func2, fabs(zicm), fabs(z)))
				            *(0.5*(1.0+cos(pi*(drcyl*LengthUnits-SmoothRadius*Mpc)/(SmoothLength*Mpc))));
				bulgeComp = (DiskGravityStellarBulgeMass==0.0?0.0:qromb(func3, fabs(zicm2), fabs(z)));
    		Pressure2 = (bulgeComp + qromb(func4, fabs(zicm2), fabs(z)))
				            *(0.5*(1.0+cos(pi*(r2-SmoothRadius*Mpc)/(SmoothLength*Mpc))));
			} // end |z| < |zicm| if

  	} // end r_cyle < TruncRadius if
	} // end r_cyl < SmoothRadius if/else

	denuse = density*DensityUnits; 
	if (Pressure < 0.0 && fabs(drcyl)*LengthUnits/Mpc <= TruncRadius && fabs(z) <= fabs(zicm)) {
		fprintf(stderr,"neg pressure:  P = %e, z = %e, r = %e\n", Pressure, z/Mpc, drcyl*LengthUnits/Mpc);
	}
	if (fabs(drcyl)*LengthUnits/Mpc >= TruncRadius || fabs(zicm) <= fabs(z)){
		Pressure = 0.0;
		Pressure2 = 0.0;
		denuse = HaloGasDensity(rsph);
	}
	if (Pressure2 <= 0.0 && Pressure <= 0.0){
		Pressure = 0.0;
		Pressure2 = 0.0;
		denuse = HaloGasDensity(rsph);
	}
	if (Pressure <= 0.0) {
		Pressure = 0.0;
		Pressure2 = 0.0;
		denuse = HaloGasDensity(rsph);
	}
	if (denuse < HaloGasDensity(rsph)) {
		fprintf(stderr,"denuse small:  %e\n", denuse);
	}
	rsph_icm = sqrt(drcyl*drcyl+pow(zicm/LengthUnits,2));
	Picm = HaloGasDensity(rsph_icm)*kboltz*HaloGasTemperature(rsph_icm)/(0.6*mh);
	temperature=0.6*mh*(Picm+Pressure)/(kboltz*denuse);

	/* Calculate pressure gradient */
	FdPdR = (Pressure2 - Pressure)/(r2-drcyl*LengthUnits)/density; 

	/* Calculate Gravity = Fg_DM + Fg_StellarDisk + Fg_StellaDiskGravityStellarBulgeR */
	FtotR  = (-pi)*GravConst*DiskGravityDarkMatterDensity*pow(DiskGravityDarkMatterR*Mpc,3)/pow(rsph,3)*drcyl*LengthUnits
	         *(-2.0*atan(rsph/DiskGravityDarkMatterR/Mpc)+2.0*log(1.0+rsph/DiskGravityDarkMatterR/Mpc)
	           +log(1.0+pow(rsph/DiskGravityDarkMatterR/Mpc,2)));
	FtotR += -GravConst*DiskGravityStellarDiskMass*SolarMass*drcyl*LengthUnits
             /sqrt(pow(pow(drcyl*LengthUnits,2)+pow(DiskGravityStellarDiskScaleHeightR*Mpc
	                   +sqrt(pow(z,2)+pow(DiskGravityStellarDiskScaleHeightz*Mpc,2)),2),3));
	FtotR += -GravConst*DiskGravityStellarBulgeMass*SolarMass
	           /pow(sqrt(pow(z,2)+pow(drcyl*LengthUnits,2))+DiskGravityStellarBulgeR*Mpc,2)*drcyl*LengthUnits/sqrt(pow(z,2)
	                +pow(drcyl*LengthUnits,2));


	if (temperature < 0.0) fprintf(stderr,"temp = %g, P = %g, z = %g, zicm = %g, zicmf=%g, zsmall=%g, drcyl = %g\n", 
		temperature, Pressure, z/Mpc, zicm/Mpc, zicmf, zsmall, drcyl*LengthUnits/Mpc);
	if ((FtotR - FdPdR) > 0.0) { 
		fprintf(stderr,"FtotR = %g, FdPdR = %g, P = %g,P2 = %g, Picm = %g, dr = %g, drcyl = %g, z = %g\n", 
			FtotR, FdPdR, Pressure, Pressure2, Picm, r2-drcyl*LengthUnits, drcyl*LengthUnits/Mpc, z/Mpc);
   	FdPdR = 0.0;
	} // end FtotR - FdPdr > 0.0 if

	/* Find circular velocity by balancing FG and dPdR against centrifugal force */
	vrot=sqrt(-drcyl*LengthUnits*(FtotR-FdPdR));
	if ((denuse == densicm)) vrot = 0.0;

  return (vrot/VelocityUnits); //code units
} // end DiskPotentialCircularVelocity


// the two functions integrated by qromb

double func1(double zint)
{

  extern double drcyl;
  return (-MgasScale*SolarMass/(2*pi*pow(gScaleHeightR*Mpc,2)*gScaleHeightz*Mpc)*0.25/cosh(drcyl*LengthUnits/gScaleHeightR/Mpc)/cosh(fabs(zint)/gScaleHeightz/Mpc)*GravConst*DiskGravityStellarBulgeMass*SolarMass/pow((sqrt(pow(zint,2)+pow(drcyl*LengthUnits,2))+DiskGravityStellarBulgeR*Mpc),2)*fabs(zint)/sqrt(pow(zint,2)+pow(drcyl*LengthUnits,2)));
}

double func2(double zint)
{
  extern double drcyl;
  return (-MgasScale*SolarMass/(2*pi*pow(gScaleHeightR*Mpc,2)*gScaleHeightz*Mpc)*0.25/cosh(drcyl*LengthUnits/gScaleHeightR/Mpc)/cosh(fabs(zint)/gScaleHeightz/Mpc)*GravConst*DiskGravityStellarDiskMass*SolarMass*(DiskGravityStellarDiskScaleHeightR*Mpc+sqrt(pow(zint,2)+pow(DiskGravityStellarDiskScaleHeightz*Mpc,2)))*fabs(zint)/sqrt(pow(pow(drcyl*LengthUnits,2)+pow((DiskGravityStellarDiskScaleHeightR*Mpc+sqrt(pow(zint,2)+pow(DiskGravityStellarDiskScaleHeightz*Mpc,2))),2),3))/sqrt(pow(zint,2)+pow(DiskGravityStellarDiskScaleHeightz*Mpc,2)));
}

// need to repeat the same functions but with a new r

double func3(double zint)
{
  extern double r2;
  return (-MgasScale*SolarMass/(2*pi*pow(gScaleHeightR*Mpc,2)*gScaleHeightz*Mpc)*0.25/cosh(r2/gScaleHeightR/Mpc)/cosh(fabs(zint)/gScaleHeightz/Mpc)*GravConst*DiskGravityStellarBulgeMass*SolarMass/pow((sqrt(pow(zint,2)+pow(r2,2))+DiskGravityStellarBulgeR*Mpc),2)*fabs(zint)/sqrt(pow(zint,2)+pow(r2,2)));
}

double func4(double zint)
{
  extern double r2;
  return (-MgasScale*SolarMass/(2*pi*pow(gScaleHeightR*Mpc,2)*gScaleHeightz*Mpc)*0.25/cosh(r2/gScaleHeightR/Mpc)/cosh(fabs(zint)/gScaleHeightz/Mpc)*GravConst*DiskGravityStellarDiskMass*SolarMass*(DiskGravityStellarDiskScaleHeightR*Mpc+sqrt(pow(zint,2)+pow(DiskGravityStellarDiskScaleHeightz*Mpc,2)))*fabs(zint)/sqrt(pow(pow(r2,2)+pow(DiskGravityStellarDiskScaleHeightR*Mpc+sqrt(pow(zint,2)+pow(DiskGravityStellarDiskScaleHeightz*Mpc,2)),2),3))/sqrt(pow(zint,2)+pow(DiskGravityStellarDiskScaleHeightz*Mpc,2)));
}

// Will be called by qromb to find the pressure at every point in disk.

#define FUNC(x) ((*func)(x))

double trapzd(double (*func)(double), double a, double b, int n)
{
	static double s;
	static int it;
	int j;
	double del, sum, tnm, x;

	if (n == 1){
		it = 1;
		return (s=0.5*(b-a)*(FUNC(a)+FUNC(b)));
	}

	tnm = it;
	del = (b-a)/tnm;
	x = a+0.5*del;
	for (sum=0.0,j=1;j<=it;j++,x+=del) sum += FUNC(x);
	it *= 2;
	s = 0.5*(s+(b-a)*sum/tnm);
	return s;
} // end trapezoid

#define K 7  // FIXME
FLOAT polint_c[K+1];
FLOAT polint_d[K+1];

/* also called by qromb */
void polint(double xa[],double ya[],int n,double x,double *y,double *dy)
{
	int i,m,ns=1;
	double den,dif,dift,ho,hp,w;
	void nrerror(char *);

	dif=fabs(x-xa[1]);
	for (i=1;i<=n;i++) {
		if ( (dift=fabs(x-xa[i])) < dif) {
			ns=i;
			dif=dift;
		}
		polint_c[i]=ya[i];
		polint_d[i]=ya[i];
	} // end i for
	
	*y=ya[ns--];
	for (m=1;m<n;m++) {
		for (i=1;i<=n;i++) {
			ho=xa[i]-x;
			hp=xa[i+m]-x;
			w=polint_c[i+1]-polint_d[i];
			if ( (den=ho-hp) == 0.0 ) fprintf(stderr,"Error in routine POLINT\n");
			den = w/den;
			polint_d[i]=hp*den;
			polint_c[i]=ho*den;
		} // end i for
		*dy=(2*ns < (n-m) ? polint_c[ns+1] : polint_d[ns--]);
		*y += (*dy);
	} // end m for
} // end polint

#define EPS 1.0e-6
#define JMAX 20
#define JMAXP JMAX+1

/* Main integration routine called by DiskPotentialCircularVelocity to find Pressure */
double qromb(double (*func)(double), double a, double b)
{
	if( a == b ) return 0.0;
	double ss,dss,trapzd(double (*func)(double), double a, double b, int n);
  int j;
  double h[JMAXP+1], s[JMAXP+1];
  void polint(double xa[],double ya[],int n,double x,double *y,double *dy),nrerror(char *);

  h[1] = 1.0;
  for (j=1;j<=JMAX;j++){
    s[j] = trapzd(func,a,b,j);
    if( isnan(s[j]) ) ENZO_FAIL("NaN's during pressure integration in GalaxySimulationInitialize");
    if (j >= K) {
      polint(&h[j-K],&s[j-K],K,0.0,&ss,&dss);
			if (fabs(dss) < EPS*fabs(ss)) return ss;
    }
    s[j+1]=s[j];
    h[j+1]=0.25*h[j]; 
  }
	/* Print bug report and exit */
  fprintf(stderr,"Too many steps in routine QROMB\n");
  fprintf(stderr,"\t>> drcyl = %e, z = %e, z_icm = %e\n", drcyl*LengthUnits/Mpc, a/Mpc, b/Mpc);
	fprintf(stderr,"\t>> ss = %e, dss = %e\n", ss, dss);
	ENZO_FAIL("FAILED IN QROMB IN GRID_GALAXYSIMULATIONINIALIZE\n");
	return -1.0;
}
