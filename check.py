import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
import astro.galaxy as gal
import astro.cgs as cgs

halo_model = 'hydrostatic'
#halo_model = 'uniform'


def halo_profiles(r):
	dark_matter = gal.burkert_mass(r)
	kwargs = { 'M' : dark_matter, 'gamma' : 1.0 }
	temperature = gal.halo_gas_temperature(r,**kwargs)
	r0 = 2.0*cgs.kpc; T0 = interp1d(r,temperature)(r0)
	density = gal.lmc_halo_gas_density(r,r0=r0,T0=T0,**kwargs)
	pressure = cgs.P(density,temperature)
	return ( dark_matter , temperature , density , pressure )

# ------- CHECK DISK DENSITY FUNCTION ...
data = np.loadtxt('data/density.out')
z = data[:,0]
Nr = 5
r = np.linspace(0.0,0.00769231,5)*cgs.Mpc

for i in range(1,6):
	plt.loglog(z,data[:,i])

labels = []
for rr in r:
	plt.loglog(z,gal.gas_disk_density(rr,z),'k--')
	labels.append("R = %.2f kpc" % (rr/cgs.kpc))

plt.ylim((1e-26,1e-23))
plt.title('Disk Gas Density')
plt.legend(labels,loc='best')
plt.savefig('figures/disk_density.eps')

data = np.loadtxt('data/zicm.out')


# ------- VERIFY METHOD OF BISECTION WORKS
plt.clf()
z = np.logspace(-2,1,100)*cgs.kpc
radii = data[:,0]
heights = data[:,1]
labels = []
colors = [ 'b', 'g', 'r', 'c', 'm' ]
for r,c in zip(radii,colors):
	plt.semilogy(z/cgs.kpc,gal.gas_disk_density(r,z),color=c)
	labels.append("r = %.2f kpc" % (r/cgs.kpc) )


# find T0
R0 = 2.0*cgs.kpc; 
RR = np.linspace(1.0,3.0,3.0)*R0
kwargs = {'M':gal.burkert_mass(RR),'gamma':1.0}
T0 = interp1d(RR,gal.halo_gas_temperature(RR,**kwargs))(R0)


for r,h,c in zip(radii,heights,colors):

	R = np.sqrt(r**2+z**2)
	kwargs = {'M':gal.burkert_mass(R),'gamma':1.0}
	halo_density = gal.lmc_halo_gas_density(R,r0=R0,T0=T0,**kwargs)

	if halo_model == 'hydrostatic':
		plt.semilogy(z/cgs.kpc,halo_density,'-.',linewidth=.5,color=c)
	plt.semilogy(h/cgs.kpc,gal.gas_disk_density(r,np.array([h])),'k.',markersize=10)

if halo_model == 'uniform':
	plt.semilogy(z/cgs.kpc,np.ones(z.shape)*1.84563e-29,'k-.',linewidth=.5)
plt.axis((1.0,6.0,1e-30,1e-26))
plt.legend(labels,loc='best')
plt.xlabel('z_icm [ kpc ]')
plt.ylabel('density [ cgs ]')
plt.title('test of bisection method - ' + halo_model + ' model')
plt.savefig('figures/bisect.eps')

# --------- VERIFY HALO PROFILES LOOK GOOD

# load in C++ data
data = np.loadtxt('data/halo.out')
r = data[:,0]

def P(rho,T):
	return rho*cgs.kb*T/(cgs.mu*cgs.mp)

# generate python data
dark_matter = gal.burkert_mass(r)/cgs.Msun
kwargs = { 'M' : data[:,1], 'gamma' : 1.0 }
temperature = gal.halo_gas_temperature(r,**kwargs)
r0 = 2.0*cgs.kpc; T0 = interp1d(r,data[:,2])(r0)
density = gal.lmc_halo_gas_density(r,r0=r0,T0=T0,**kwargs)
pressure = cgs.P(density,temperature)

# plot, comparing to python functions ...
plt.clf()
f , ((ax1,ax2),(ax3,ax4)) = plt.subplots(2,2)
plots = { 
	ax1 : ( 'M_DM(R) [ Msun ]'        , data[:,1]/cgs.Msun         , dark_matter ), 
	ax2 : ( 'Temperature [ K ]'       , data[:,2]                  , temperature ),
	ax3 : ( 'Gas Density [ g/cm^3 ]'  , data[:,3]                  , density     ),
	ax4 : ( 'Pressure [ dynes/cm^2 ]' , cgs.P(data[:,3],data[:,2]) , pressure    )
}
for ax , (label,cdata,pdata) in plots.items():
	ax.semilogy(r/cgs.kpc,cdata,'r-',linewidth=2)
	ax.semilogy(r/cgs.kpc,pdata,'k--')
	ax.set_title(label)
	ax.set_xlim(np.array([r[0],r[-1]])/cgs.kpc)
for ax in (ax3,ax4):
	ax.set_xlabel('r [ kpc ]')

# add comparison to old values ...
T_old = 1199219.3517; d_old = 1.84562780554e-29
old_vals = { ax2 : T_old , ax3 : d_old , ax4 : cgs.P(d_old, T_old) }
for ax,val in old_vals.items():
	ax.semilogy( np.array([r[0],r[-1]])/cgs.kpc , [ val , val ] , 'k-.', linewidth=.5)

plt.savefig('figures/profiles.eps')


# ---- VERIFY CIRCULAR VELOCITY METHOD FOR TEMP
data = np.loadtxt('data/circular.out')

plt.clf()
f , ((ax1,ax2)) = plt.subplots(1,2)
r = data[:,0]
py_dm , py_temp , py_dens , py_press = halo_profiles(r)
plots = { ax1 : (data[:,1],py_dens) , ax2 : (data[:,2],py_temp) }
for ax , (enzo, pydat) in plots.items():
	ax.semilogy(r/cgs.kpc, enzo, 'r-',linewidth=2)
	ax.semilogy(r/cgs.kpc, pydat, 'k--')
	ax.set_xlabel('r [ kpc ]')
plt.savefig('figures/circular')
