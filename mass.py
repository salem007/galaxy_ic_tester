"""
	MASS

		Check the total mass of the LMC halo prescription 
"""
import numpy as np
from scipy.interpolate import interp1d
import astro.galaxy as gal
import astro.cgs as cgs

def halo_profiles(r):
  dark_matter = gal.burkert_mass(r)
  kwargs = { 'M' : dark_matter, 'gamma' : 1.0 }
  temperature = gal.halo_gas_temperature(r,**kwargs)
  r0 = 2.0*cgs.kpc; T0 = interp1d(r,temperature)(r0)
  density = gal.lmc_halo_gas_density(r,r0=r0,T0=T0,**kwargs)
  pressure = cgs.P(density,temperature)
  return ( dark_matter , temperature , density , pressure )


r = np.linspace(0.0,30.0,100.0)*cgs.kpc
dr = r[1:]-r[:-1]
r = r[1:]

dark_matter,temperature,density,pressure = halo_profiles(r)
dV = 4.0*np.pi*r**2*dr

mass = np.dot(density,dV)

print "%.2e" % (mass/cgs.Msun)

